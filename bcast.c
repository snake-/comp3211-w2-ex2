#include <stdio.h>
#include "mpi.h"

int main( argc, argv )
int argc;
char **argv;
{
    int rank, value, total;
    MPI_Init( &argc, &argv );
    MPI_Comm_rank( MPI_COMM_WORLD, &rank );
    MPI_Status status;
  
    if (rank == 0)
    { 
        printf("Input a number: ");
        // Flush here so the print displays properly
        fflush(stdout);
        scanf( "%d", &value );
    } 

        MPI_Bcast( &value, 1, MPI_INT, 0, MPI_COMM_WORLD );
        printf("Process %d has received the broadcast value %d\n", rank, value);

    if (rank != 0)
    {
        value = value * rank;
        MPI_Send( &value, 1, MPI_INT, 0, 0, MPI_COMM_WORLD );
    }

    MPI_Reduce(  &value, &total, 1, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD );

    if (rank == 0)
    {
        printf("The total is: %d\n", total);
    }

    MPI_Finalize( );
    return 0;
}

