/* Sending data in a ring 
* K Djemame
* September 2006
*/

#include <stdio.h>
#include "mpi.h"

int main( argc, argv )
int argc;
char **argv;
{
    int rank, value, size;
    MPI_Status status;

    MPI_Init( &argc, &argv );
    // Determine which process this is
    MPI_Comm_rank( MPI_COMM_WORLD, &rank );
    // Determine the numeber of processes
    MPI_Comm_size( MPI_COMM_WORLD, &size );

   char         processorname[100];
   int          namelen;

   MPI_Get_processor_name( processorname, &namelen );
	// If this is the first process to run then prompt user for an input	
        if (rank == 0) {
	    printf("Input a value : ");
	    // Have to flush here otherwise printf doesn't display properly
	    fflush(stdout);
            scanf( "%d", &value );
	    MPI_Send( &value, 1, MPI_INT, rank + 1, 0, MPI_COMM_WORLD );
	}
	else {
	// Every other process receives the message and sends it to the next
	    MPI_Recv( &value, 1, MPI_INT, rank - 1, 0, MPI_COMM_WORLD, 
		      &status );
            value++;
	    if (rank < size - 1)
            {
		MPI_Send( &value, 1, MPI_INT, rank + 1, 0, MPI_COMM_WORLD );
            }
	}
	// When data reaches the last process, send it back to process 0
	if (rank == size-1) {
	    MPI_Send( &value, 1, MPI_INT, 0, 0, MPI_COMM_WORLD); 
	 }
	if (rank == 0) {
	    MPI_Recv( &value, 1, MPI_INT, rank-1, 0, MPI_COMM_WORLD, &status );
	}	
	printf( "Process %d on %s got %d\n", rank, processorname, value );

    MPI_Finalize( );
    return 0;
}

